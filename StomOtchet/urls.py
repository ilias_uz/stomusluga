"""StomUsluga URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^doctors_chart/$', 'apps.stom_otchet.views.doctors_chart', name='doctors_chart'),
    url(r'^doctors_chart/(?P<id>\d+)/$', 'apps.stom_otchet.views.doctors_chart', name='personal'),
    url(r'^$', 'apps.stom_otchet.views.index', name='my_view'),
    url(r'^filter/$', 'apps.stom_otchet.views.filter', name='filter'),
    url(r'^for_to_day/$', 'apps.stom_otchet.views.for_to_day', name='for_to_day'),
    url(r'^for_to_day_table/$', 'apps.stom_otchet.views.for_to_day_table', name='for_to_day_table'),
    url(r'^doctors_table', 'apps.stom_otchet.views.doctors_tables',  name='doctors_tables'),
    url(r'^doctors_patients/(?P<did>\d+)/', 'apps.stom_otchet.views.doctors_patients', name='doctors_patients'),
    url(r'^table_filters/$', 'apps.stom_otchet.views.table_filters', name='table_filters'),
    url(r'^doctor_filters/(?P<did>\d+)/$', 'apps.stom_otchet.views.doctor_filters', name='doctor_filters'),
    url(r'^doctor_table_for_to_day/(?P<did>\d+)/$', 'apps.stom_otchet.views.doctor_table_for_to_day', name='doctor_table_for_to_day'),
    url(r'^treat_list/$', 'apps.stom_otchet.views.treat_list', name='treat_list'),
    url(r'^filter_treat/$', 'apps.stom_otchet.views.filter_treat', name='filter_treat'),
    url(r'^treat/(?P<id>\d+)/$', 'apps.stom_otchet.views.treat', name='treat'),
    url(r'^payment/', 'apps.stom_otchet.views.pay_for_week', name='payment'),
    url(r'^pay_for_week_month/', 'apps.stom_otchet.views.pay_for_week_month', name='payment_mont'),
    url(r'^download_pay_m/', 'apps.stom_otchet.views.download_pay_m', name='download_pay_m'),
    url(r'^download_posesenie/', 'apps.stom_otchet.views.download_posesenie', name='download_posesenie'),
    url(r'^logout_view/', 'apps.stom_otchet.views.logout_view', name='logout_view'),
    url(r'^documentation/(?P<id>\d+)/', 'apps.stom_otchet.views.documentation', name='documentation'),
    url(r'^$index/', 'apps.stom_otchet.views.choce_clinik', name='choce_clinik'),
    url(r'^doctors_index/', 'apps.stom_otchet.views.doctors_index', name='doctors_index'),
    url(r'^goals/', 'apps.stom_otchet.views.goals', name='goals'),
    url(r'^goals_for_year/', 'apps.stom_otchet.views.goals_for_year', name='goals_for_year'),
    url(r'^treatment/', 'apps.stom_otchet.views.treatment', name='treatment'),
    url(r'^(?P<db_name>\w+)/', 'apps.stom_otchet.views.clinik', name='clinik'),
    url(r'^doctors_index_filter/', 'apps.stom_otchet.views.doctors_index_filter', name='doctors_index_filter'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) +\
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# if settings.DEBUG is False:
#     urlpatterns +=\
#         static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) +\
#         static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#
# if settings.DEBUG is True:
#     urlpatterns +=\
#         static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) +\
#         static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)