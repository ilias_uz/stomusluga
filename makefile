
run:
	./manage.py runserver
migrate:
	./manage.py makemigrations
	./manage.py migrate
base_commands:
	$ chmod +x manage.py
	$ sudo apt-get update
	$ ./check_install_package
	$ pip install -r requirements.txt
	$ ./manage.py migrate
	$ ./manage.py createsuperuser
	$ gunicorn StomOtchet.wsgi:application -b 127.0.0.1:8000
development:
	./manage.py runserver --settings=StomOtchety.settings.settings.py

