# -*- coding: utf-8 -*-
import csv

from django.views.generic import View
from reportlab.pdfgen import canvas
from django.utils.translation import ugettext
from django.contrib.auth import logout
# from admin import PosesenieResource
from django.contrib.auth import authenticate, login as login
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.defaultfilters import register
from datetime import date, datetime
from .models import Posesenie, Personal, TreatTreat, Payment, Documentation, Zurnal, Chair, Kadry, Goals, PriceCategory, \
    Price
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils import timezone
import json
from django.db.models import Sum
from django import template
import calendar
from io import BytesIO
from PIL import Image
from transliterate import translit, get_available_language_codes
# from excel_response import ExcelResponse
register = template.Library()


month_name_dic = {
    'January': 1,
    'February': 2,
    'March': 3,
    'April': 4,
    'May': 5,
    'June': 6,
    'July': 7,
    'August': 8,
    'September': 9,
    'October': 10,
    'November': 11,
    'December': 12
}


# This function using inside payment_fun
def function_for_get_payment_for_month_on_weeks(personals, kassa, nalich, totals, month, year, bank, location):
    days = 0
    sum_bank = 0
    dollar_bank = 0
    while days <= 28:
        kassa_total_sum_d = 0
        kassa_total_sum_s = 0
        nal_total_sum_d = 0
        nal_total_sum_s = 0
        for personal in personals:
            payment = Payment.objects.filter(Date__month=month, Date__year=year,
                                             Date__day__gte=days, Date__day__lt=days+7,
                                             PID=personal.DID, location=location)
            sums = 0
            nal = 0
            dollar_kassa = 0
            dollar_nal = 0
            for payment in payment:
                if payment.Method == u'Касса':
                    sums += payment.DefaultCurrency
                    dollar_kassa += payment.ForeignCurrency
                    kassa_total_sum_d += payment.ForeignCurrency
                    kassa_total_sum_s += payment.DefaultCurrency
                if payment.Method == u'Наличные':
                    nal += payment.DefaultCurrency
                    dollar_nal += payment.ForeignCurrency
                    nal_total_sum_d += payment.ForeignCurrency
                    nal_total_sum_s += payment.DefaultCurrency
                if payment.Method == u'Банк':
                    sum_bank += payment.DefaultCurrency
                    dollar_bank += payment.ForeignCurrency
            kassa.append([days, days+7, sums, personal.DI, dollar_kassa])
            nalich.append([days, days+7, nal, personal.DI, dollar_nal])
        totals.append([kassa_total_sum_s, kassa_total_sum_d, nal_total_sum_s, nal_total_sum_d])
        days += 7
    bank.append([dollar_bank, sum_bank])
    return kassa, nalich, totals, bank


# This function using inside payment_fun
def get_total_payment_by_payment_method(personals, pers_sum, all_total, month, year, subtotal, totals, bank, dollar, location):
    kassa_total_pers_d = 0
    kassa_total_pers_s = 0
    nal_total_pers_d = 0
    nal_total_pers_s = 0
    for pers_name in personals:
        pers_sum_month_kassa_d = 0
        pers_sum_month_kassa_s = 0
        pers_sum_month_nal_d = 0
        pers_sum_month_nal_s = 0
        pers_sum_moth_bank_d = 0
        pers_sum_moth_bank_s = 0
        all_total_d = 0
        all_total_s = 0
        payment = Payment.objects.filter(Date__month=month, Date__year=year, PID=pers_name.DID, location=location)
        for payment_month in payment:
            all_total_d += payment_month.ForeignCurrency
            all_total_s += payment_month.DefaultCurrency
            if payment_month.Method == u'Касса':
                pers_sum_month_kassa_d += payment_month.ForeignCurrency
                pers_sum_month_kassa_s += payment_month.DefaultCurrency
                kassa_total_pers_d += payment_month.ForeignCurrency
                kassa_total_pers_s += payment_month.DefaultCurrency
            if payment_month.Method == u'Наличные':
                pers_sum_month_nal_d += payment_month.ForeignCurrency
                pers_sum_month_nal_s += payment_month.DefaultCurrency
                nal_total_pers_d += payment_month.ForeignCurrency
                nal_total_pers_s += payment_month.DefaultCurrency
            if payment_month.Method == u'Банк':
                pers_sum_moth_bank_d += payment_month.ForeignCurrency
                pers_sum_moth_bank_s += payment_month.DefaultCurrency
        subtotal_sum_kassa_d = pers_sum_month_kassa_d + pers_sum_moth_bank_d
        subtotal_sum_kassa_s = pers_sum_month_kassa_s + pers_sum_moth_bank_s
        pers_sum.append([subtotal_sum_kassa_d, subtotal_sum_kassa_s, pers_sum_month_nal_d, pers_sum_month_nal_s,
                         pers_name.DI, pers_sum_moth_bank_d, pers_sum_moth_bank_s])
        all_total_sum_to_dollar = all_total_s / dollar
        all_total_dollar = all_total_sum_to_dollar + all_total_d
        all_total_dollar_to_sum = all_total_d * dollar
        all_total_sum = all_total_dollar_to_sum + all_total_s
        all_total.append([round(all_total_dollar), round(all_total_sum), pers_name.DI])

    total_sum = 0
    total_all_dollar = 0
    for all_total in all_total:
        total_sum += all_total[1]
        total_all_dollar += all_total[0]

    total_sum_to_dollar = total_sum / dollar

    subtotal_sum_kassa = 0
    subtotal_dollar_kassa = 0
    subtotal_sum_nal = 0
    subtotal_dollar_nal = 0

    for totals in totals:
        subtotal_sum_kassa += totals[0]
        subtotal_dollar_kassa += totals[1]
        subtotal_dollar_nal += totals[3]
        subtotal_sum_nal += totals[2]

    for bank in bank:
        subtotal_dollar_kassa += bank[0]
        subtotal_sum_kassa += bank[1]

    sum_to_dollar_kassa = subtotal_sum_kassa / dollar
    dollar_plus_sum_kassa = sum_to_dollar_kassa + subtotal_dollar_kassa


    total_kgs = subtotal_sum_nal + subtotal_sum_kassa
    total_dollar = subtotal_dollar_kassa + subtotal_dollar_nal
    sum_to_dollar_nal = nal_total_pers_s / dollar
    dollar_plus_sum_nal = sum_to_dollar_nal + nal_total_pers_d

    full_total = dollar_plus_sum_kassa + dollar_plus_sum_nal

    subtotal.append([subtotal_sum_kassa, subtotal_dollar_kassa, round(dollar_plus_sum_kassa), subtotal_sum_nal,
                     subtotal_dollar_nal, total_kgs, total_dollar, total_sum, total_all_dollar,
                     round(total_sum_to_dollar), round(dollar_plus_sum_nal), round(full_total)])
    return pers_sum, all_total, subtotal


def payment_fun(request, month, year, dollar):
    location = request.session['location']
    personals = Personal.objects.filter(location=location)
    month_name = calendar.month_name[month]
    kassa = []
    nalich = []
    totals = []
    bank = []

    # Her function for monthly payment on weekly for show on the html table it return kassa, nalich, totals
    function_for_get_payment_for_month_on_weeks(personals, kassa, nalich, totals, month, year, bank, location)

    all_total = []
    pers_sum = []
    subtotal = []

    # Get all payment from payment by Method and give. Get all total dollar (ForeignCurrency) and sum (DefaultCurrency)
    get_total_payment_by_payment_method(personals, pers_sum, all_total, month, year, subtotal, totals, bank, dollar, location)

    payment_total_sum = Payment.objects.filter(Date__month__gte=1, Date__month__lte=month, Date__year__gte=2015,
                                               Date__year__lte=year)
    kassa_total_d = 0
    kassa_total_s = 0
    nal_total_d = 0
    nal_total_s = 0
    total_sum = []
    for payment_total in payment_total_sum:
        if payment_total.Method == u'Касса':
            kassa_total_d += payment_total.ForeignCurrency
            kassa_total_s += payment_total.DefaultCurrency
        if payment_total.Method == u'Наличные':
            nal_total_d += payment_total.ForeignCurrency
            nal_total_s += payment_total.DefaultCurrency

    nal_sum_to_dollar = nal_total_s / dollar
    all_sum_dollar_nal = nal_sum_to_dollar + nal_total_d

    kassa_sum_to_dollar = kassa_total_s / dollar
    all_sum_dollar_kassa = kassa_sum_to_dollar + nal_total_d

    all_sum_to_dollar = all_sum_dollar_nal + all_sum_dollar_kassa

    total_sum.append([round(all_sum_to_dollar), round(all_sum_dollar_kassa), round(all_sum_dollar_nal)])
    return render(request, 'payment.html', {'payments': kassa, 'pers': personals, 'nalich': nalich, 'totals': totals,
                                            'pers_sum': pers_sum, 'total_sum': total_sum, 'month_name': month_name,
                                            'all_total': all_total, 'bank': bank, 'subtotal': subtotal})


def doctors_chart(request):
    request.session['location'] = request.POST.get('clinic_name')
    location = request.session['location']
    posessenie = Posesenie.objects.filter(location=location)
    kadry = Kadry.objects.filter(location=location)
    personal = Personal.objects.all()
    return render(request, 'doctors_chart.html', {'posessenie': posessenie, 'personal': personal, 'kadry': kadry})


def doctors_tables(request):
    posessenie = Posesenie.objects.all().order_by('-Data')
    personal = Personal.objects.all()
    kadri = Kadry.objects.all()
    paginator = Paginator(posessenie, 20)
    page = request.GET.get('page')
    all_posessenie = Posesenie.objects.all()
    try:
        posessenie = paginator.page(page)
    except PageNotAnInteger:
        posessenie = paginator.page(1)
    except EmptyPage:
        posessenie = paginator.page(paginator.num_pages)
    return render(request, 'doctors_table.html', {'posessenie': posessenie, 'personal': personal, 'all_posessenie': all_posessenie,
                                                  'kadri': kadri})


def index(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if request.user.is_authenticated():
        return render(request, 'index.html')
    if user is not None:
        if user.is_active:
            if user.is_superuser:
                login(request, user)
                return render(request, 'index.html')
            elif not user.is_superuser:
                login(request, user)
                return redirect('/doctors_index')
        else:
            return HttpResponse("Invalid login details supplied")
    else:
        return render(request, 'login.html')


def logout_view(request):
    logout(request)
    return redirect('/')


def filter(request):
    location = request.session['location']
    date_from = request.POST.get('date-from')
    date_to = request.POST.get('date-to')
    kadri = Kadry.objects.all()
    # split date-from
    date_from_for_filter = date_from.split('/')
    date_from_moth_int = int(date_from_for_filter[0])
    date_from_day_int = int(date_from_for_filter[1])
    date_from_year_int = int(date_from_for_filter[2])
    # split date-to
    date_to_for_filter = date_to.split('/')
    date_to_moth_int = int(date_to_for_filter[0])
    date_to_day_int = int(date_to_for_filter[1])
    date_to_year_int = int(date_to_for_filter[2])
    # filtering date from and date to in posesenie
    posessenie = Posesenie.objects.filter(data__gte=date(date_from_year_int, date_from_moth_int, date_from_day_int),
                                          data__lte=date(date_to_year_int, date_to_moth_int, date_to_day_int), location=location)
    personal = Personal.objects.all()
    return render(request, 'doctors_chart.html', {'posessenie': posessenie, 'personal': personal, 'kadry': kadri})


def doctors_patients(request, did):
    posessenie = Posesenie.objects.filter(Personal=did).order_by('-Data')
    treat_id = TreatTreat.objects.all()
    kadri = Kadry.objects.all()
    personal_name = Personal.objects.get(DID=did)
    all_personal = Personal.objects.all()
    treat = TreatTreat.objects.all()
    paginator = Paginator(posessenie, 15)
    page = request.GET.get('page')
    all_posessenie = Posesenie.objects.filter(Personal=did).order_by('-Data')
    try:
        posessenie = paginator.page(page)
    except PageNotAnInteger:
        posessenie = paginator.page(1)
    except EmptyPage:
        posessenie = paginator.page(paginator.num_pages)
    return render(request, 'doctors-patients.html', {'posessenie': posessenie, 'personal_name': personal_name,
                                                     'personal': all_personal, 'treat': treat, 'all_posessenie': all_posessenie,
                                                     'treat_id': treat_id, 'kadri': kadri})


def table_filters(request):
    location = request.session['location']
    date_from = request.POST.get('date-from')
    date_to = request.POST.get('date-to')
    # split date-from
    date_from_for_filter = date_from.split('/')
    date_from_moth_int = int(date_from_for_filter[0])
    date_from_day_int = int(date_from_for_filter[1])
    date_from_year_int = int(date_from_for_filter[2])
    # split date-to
    date_to_for_filter = date_to.split('/')
    date_to_moth_int = int(date_to_for_filter[0])
    date_to_day_int = int(date_to_for_filter[1])
    date_to_year_int = int(date_to_for_filter[2])
    # filtering date from and date to in posesenie
    posessenie = Posesenie.objects.filter(data__gte=date(date_from_year_int, date_from_moth_int, date_from_day_int),
                                          data__lte=date(date_to_year_int, date_to_moth_int, date_to_day_int),
                                          location=location)
    personal = Personal.objects.all()
    all_posessenie = Posesenie.objects.filter(data__gte=date(date_from_year_int, date_from_moth_int, date_from_day_int),
                                              data__lte=date(date_to_year_int, date_to_moth_int, date_to_day_int),
                                              location=location)
    return render(request, 'doctors_table.html', {'posessenie': posessenie, 'personal': personal,
                                                  'all_posessenie': all_posessenie})


def doctor_filters(request, did):
    location = request.session['location']
    date_from = request.POST.get('date-from')
    date_to = request.POST.get('date-to')
    kadri = Kadry.objects.all()
    # split date-from
    date_from_for_filter = date_from.split('/')
    date_from_moth_int = int(date_from_for_filter[0])
    date_from_day_int = int(date_from_for_filter[1])
    date_from_year_int = int(date_from_for_filter[2])
    # split date-to
    date_to_for_filter = date_to.split('/')
    date_to_moth_int = int(date_to_for_filter[0])
    date_to_day_int = int(date_to_for_filter[1])
    date_to_year_int = int(date_to_for_filter[2])
    # filtering date from and date to in posesenie
    personal_id = Personal.objects.get(pk=did)
    posessenie = Posesenie.objects.filter(data__gte=date(date_from_year_int, date_from_moth_int, date_from_day_int),
                                          data__lte=date(date_to_year_int, date_to_moth_int, date_to_day_int),
                                          personal=personal_id, location=location)
    all_posessenie = Posesenie.objects.filter(data__gte=date(date_from_year_int, date_from_moth_int, date_from_day_int),
                                          data__lte=date(date_to_year_int, date_to_moth_int, date_to_day_int),
                                          personal=personal_id, location=location)
    personal = Personal.objects.all(location=location)
    personal_name = Personal.objects.get(pk=did, location=location)
    return render(request, 'doctors-patients.html', {'posessenie': posessenie, 'personal': personal,
                                                     'personal_name': personal_name, 'all_posessenie': all_posessenie,
                                                     'kadri': kadri})


def for_to_day(request):
    location = request.session['location']
    posessenie = Posesenie.objects.filter(data=timezone.now(), location=location)
    kadri = Kadry.objects.filter(location=location)
    personal = Personal.objects.filter(location=location)
    return render(request, 'doctors_chart.html', {'posessenie': posessenie, 'personal': personal, 'kadry': kadri})


def for_to_day_table(request):
    location = request.session['location']
    posessenie = Posesenie.objects.filter(data=timezone.now(), location=location)
    personal = Personal.objects.filter(location=location)
    paginator = Paginator(posessenie, 15)
    page = request.GET.get('page')
    all_posessenie = Posesenie.objects.filter(data=timezone.now(), location=location)
    try:
        posessenie = paginator.page(page)
    except PageNotAnInteger:
        posessenie = paginator.page(1)
    except EmptyPage:
        posessenie = paginator.page(paginator.num_pages)
    return render(request, 'doctors_table.html', {'posessenie': posessenie, 'personal': personal, 'all_posessenie': all_posessenie})


def doctor_table_for_to_day(request, did):
    personal_id = Personal.objects.get(pk=did)
    posessenie = Posesenie.objects.filter(data=timezone.now(), personal=personal_id)
    personal = Personal.objects.all()
    kadri = Kadry.objects.all()
    personal_name = Personal.objects.get(pk=did)
    paginator = Paginator(posessenie, 15)
    page = request.GET.get('page')
    try:
        posessenie = paginator.page(page)
    except PageNotAnInteger:
        posessenie = paginator.page(1)
    except EmptyPage:
        posessenie = paginator.page(paginator.num_pages)
    return render(request, 'doctors-patients.html', {'posessenie': posessenie, 'personal': personal, 'personal_name': personal_name,
                                                     'kadri': kadri})


def treat_list(request):
    personal = Personal.objects.all()
    treat = TreatTreat.objects.all()
    # treat_for_patsient = TreatTreat.objects.all()
    kadri = Kadry.objects.all()
    paginator = Paginator(treat, 15)
    page = request.GET.get('page')
    try:
        treat = paginator.page(page)
    except PageNotAnInteger:
        treat = paginator.page(1)
    except EmptyPage:
        treat = paginator.page(paginator.num_pages)
    return render(request, 'treat.html', {'treat': treat, 'personal': personal, 'kadri': kadri})


def filter_treat(request):
    pi_pf = request.POST.get('pi_pf')
    kadri = Kadry.objects.all()
    treat = TreatTreat.objects.filter(posesenie__patient__pi=pi_pf)
    personal = Personal.objects.all()
    return render(request, 'treat_filter.html', {'treat': treat, 'personal': personal, 'kadri': kadri})


def treat(request, id):
    personal = Personal.objects.all()
    treat_for_patsient = TreatTreat.objects.all()
    kadri = Kadry.objects.all()
    posessenie = Posesenie.objects.get(pk=id)
    return render(request, 'treat.html', {'treat_for_patsient': treat_for_patsient, 'posessenie': posessenie,
                                          'personal': personal, 'kadri': kadri})


def pay_for_week(request):
    dollar = request.POST.get('weekly_shield_dollar')
    dates = datetime.now()
    payment_latest = Payment.objects.latest('Date')
    latest_month = payment_latest.Date.month
    fun = payment_fun(request, latest_month, dates.year, int(dollar))
    return fun


def pay_for_week_month(request):
    month_name = request.POST.get('month')
    year = request.POST.get('year')
    dollar = request.POST.get('dollar')
    month = month_name_dic[month_name]
    fun = payment_fun(request, month, int(year), int(dollar))
    return fun


def download_pay_m(request):
    if request.user.is_authenticated():
        dataset = Payment.objects.all()
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="payment_met.pdf"'
        buffer = BytesIO()
        p = canvas.Canvas(response)
        dollar = 0
        summa = 0
        n = 1
        for data_sum in dataset:
            dollar += data_sum.ForeignCurrency
            summa += data_sum.DefaultCurrency
        s_dollar = u'Общая сумма доллора: %s' % dollar
        s_summa = u'Общая сумма сома: %s' % summa
        page_number = 'page %s' % n
        img = Image.open('/home/iliyaz/stomotchyoty/static/images/karisma-1.gif')
        p.drawInlineImage(img, 30, 740, width=500, height=80)
        p.drawString(30, 710, translit(s_dollar, 'ru', reversed=True))
        p.drawString(220, 710, translit(s_summa, 'ru', reversed=True))
        p.drawString(450, 710, '%s' % page_number)
        p.drawString(10, 680, translit(u'Дата', 'ru', reversed=True))
        p.drawString(80, 680, 'ID')
        p.drawString(110, 680, translit(u'Имя потциента', 'ru', reversed=True))
        p.drawString(240, 680, translit(u'Скидка', 'ru', reversed=True))
        p.drawString(280, 680, translit(u'Обшая Сом', 'ru', reversed=True))
        p.drawString(365, 680, translit(u'Обшая Доллар', 'ru', reversed=True))
        # p.drawString(390, 680, translit(u'Посишение', 'ru', reversed=True))
        p.drawString(450, 680, translit(u'Сом', 'ru', reversed=True))
        p.drawString(485, 680, translit(u'Доллар', 'ru', reversed=True))
        p.drawString(520, 680, translit(u'Метод оплати', 'ru', reversed=True))
        a = 660
        for dataset in dataset:
            p.drawString(10, a, '%s' % dataset.Date)
            p.drawString(80, a, '%s' % dataset.ID)
            p.drawString(110, a, translit(dataset.PID, 'ru', reversed=True))
            p.drawString(240, a, '%s' % dataset.Discount)
            p.drawString(280, a, '%s' % dataset.Total)
            p.drawString(365, a, '%s' % dataset.TotalDollar)
            p.drawString(450, a, '%s' % dataset.DefaultCurrency)
            p.drawString(485, a, '%s' % dataset.ForeignCurrency)
            p.drawString(520, a, translit(dataset.Method, 'ru', reversed=True))
            if a == 100:
                p.showPage()
                n += 1
                page_number = 'page %s' % n
                s_dollar = u'Общая сумма доллора: %s' % dollar
                s_summa = u'Общая сумма сома: %s' % summa
                p.drawString(30, 810, translit(s_dollar, 'ru', reversed=True))
                p.drawString(220, 810, translit(s_summa, 'ru', reversed=True))
                p.drawString(450, 810, '%s' % page_number)
                p.drawString(10, 780, translit(u'Дата', 'ru', reversed=True))
                p.drawString(80, 780, 'ID')
                p.drawString(110, 780, translit(u'Имя потциента', 'ru', reversed=True))
                p.drawString(240, 780, translit(u'Скидка', 'ru', reversed=True))
                p.drawString(280, 780, translit(u'Обшая Сом', 'ru', reversed=True))
                p.drawString(365, 780, translit(u'Обшая Доллар', 'ru', reversed=True))
                # p.drawString(390, 780, translit(u'Посишение', 'ru', reversed=True))
                p.drawString(450, 780, translit(u'Сом', 'ru', reversed=True))
                p.drawString(485, 780, translit(u'Доллар', 'ru', reversed=True))
                p.drawString(520, 780, translit(u'Метод оплати', 'ru', reversed=True))
                a = 780
            a -= 20
        p.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)

        return response
    else:
        return redirect('/')


def download_posesenie(request, atype="custom"):
    if request.user.is_authenticated():
        dataset = Posesenie.objects.all()
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=posseine.pdf'
        kadry = Kadry.objects.all()
        buffer = BytesIO()
        p = canvas.Canvas(response)
        dollar = 0
        summa = 0
        patdienti = 0
        n = 1
        for data_sum in dataset:
            patdienti += 1
            if data_sum.summa_dollar and data_sum.summa != 'None':
                dollar += data_sum.summa_dollar
                summa += data_sum.summa
        s_dollar = u'Общая сумма доллора: %s' % dollar
        s_summa = u'Общая сумма сома: %s' % summa
        k_potsientov = u'Общая кол патцентов: %s' % patdienti
        page_number = 'page %s' % n
        img = Image.open('/home/iliyaz/stomotchyoty/static/images/karisma-1.gif')
        p.drawInlineImage(img, 30, 740, width=500, height=80)
        p.drawString(450, 730, '%s' % page_number)
        p.drawString(30, 710, translit(s_dollar, 'ru', reversed=True))
        p.drawString(220, 710, translit(s_summa, 'ru', reversed=True))
        p.drawString(400, 710, translit(k_potsientov, 'ru', reversed=True))
        p.drawString(30, 680, translit(u'Дата', 'ru', reversed=True))
        p.drawString(110, 680, translit(u'Имя Потциента', 'ru', reversed=True))
        p.drawString(260, 680, translit(u'Имя Доктора', 'ru', reversed=True))
        p.drawString(400, 680, translit(u'Доллар', 'ru', reversed=True))
        p.drawString(470, 680, translit(u'Сом', 'ru', reversed=True))
        a = 660
        for dataset in dataset:
            for kadry_id in kadry:
                if kadry_id.did.did == dataset.personal.did and kadry_id.real == 1:
                    d_name = '%s %s' % (dataset.personal.di, dataset.personal.df)
                    p_name = '%s %s' % (dataset.patient.pi, dataset.patient.pf)
                    p.drawString(30, a, '%s' % dataset.data)
                    p.drawString(110, a, translit(p_name, 'ru', reversed=True))
                    p.drawString(260, a, translit(d_name, 'ru', reversed=True))
                    p.drawString(400, a, '%s' % dataset.summa_dollar)
                    p.drawString(470, a, '%s' % dataset.summa)
                    if a == 100:
                        p.showPage()
                        n += 1
                        page_number = 'page %s' % n
                        p.drawString(450, 825, '%s' % page_number)
                        p.drawString(30, 810, translit(s_dollar, 'ru', reversed=True))
                        p.drawString(220, 810, translit(s_summa, 'ru', reversed=True))
                        p.drawString(400, 810, translit(k_potsientov, 'ru', reversed=True))
                        p.drawString(30, 780, translit(u'Дата', 'ru', reversed=True))
                        p.drawString(110, 780, translit(u'Имя Потциента', 'ru', reversed=True))
                        p.drawString(260, 780, translit(u'Имя Доктора', 'ru', reversed=True))
                        p.drawString(400, 780, translit(u'Доллар', 'ru', reversed=True))
                        p.drawString(470, 780, translit(u'Сом', 'ru', reversed=True))
                        a = 780
                    a -= 20
        p.save()

        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response
    else:
        return redirect('/')


def documentation(request,id):
    documentation = Documentation.objects.get(pk=id)
    doc_menu = Documentation.objects.all()
    return render(request, 'documentation.html', {'documentation': documentation, 'doc_menu': doc_menu})


def choce_clinik(request):
    return render(request, 'choce_clinik.html')


def clinik(request, db_name):
    return db_name


def doctors_index(request):
    chair = Chair.objects.all()
    zurnal = Zurnal.objects.filter(time__gte=datetime.now())
    date = datetime.now()
    return render(request, 'zurnal.html', {'zurnal': zurnal, 'chair': chair, 'date': date})


def doctors_index_filter(request):
    date_from = request.POST.get('date-from')
    date_to = request.POST.get('date-to')
    dates = date_from + '|' + date_to
    # split date-from
    date_from_for_filter = date_from.split('/')
    date_from_moth_int = int(date_from_for_filter[0])
    date_from_day_int = int(date_from_for_filter[1])
    date_from_year_int = int(date_from_for_filter[2])
    # split date-to
    date_to_for_filter = date_to.split('/')
    date_to_moth_int = int(date_to_for_filter[0])
    date_to_day_int = int(date_to_for_filter[1])
    date_to_year_int = int(date_to_for_filter[2])
    # filtering date from and date to in posesenie
    chair = Chair.objects.all()
    zurnal = Zurnal.objects.filter(time__gte=date(date_from_year_int, date_from_moth_int, date_from_day_int),
                                   time__lte=date(date_to_year_int, date_to_moth_int, date_to_day_int))
    return render(request, 'zurnal.html', {'zurnal': zurnal, 'chair': chair, 'date': dates})


def goals(request):
    location = request.session['location']
    dates = datetime.now()
    goals = Goals.objects.filter(location=location).order_by('-id')[:2]
    reversed_goal = reversed(goals)
    payment_latest = Payment.objects.latest('Date')
    latest_month = payment_latest.Date.month
    current_year = dates.year
    day = 0
    arr_for_push_sum_value = []
    arr_for_push_dollar_value = []
    sum_till_this_week = 0
    dollar_till_this_week = 0
    average = 0
    goal = 0
    week = 0
    for n in reversed_goal:
        if n.name == 'Goal':
            goal += n.value
        elif n.name == 'Average':
            average += n.value
    while day <= 28:
        week += 1
        sum_for_week = 0
        dollar_for_week = 0
        payment_obj = Payment.objects.filter(Date__day__gte=day, Date__day__lt=day+6, Date__month=10,
                                             Date__year=int(current_year), location=location)
        for payment_obj in payment_obj:
            sum_till_this_week += payment_obj.DefaultCurrency
            dollar_for_week += payment_obj.ForeignCurrency
            sum_for_week += payment_obj.DefaultCurrency
            dollar_till_this_week += payment_obj.ForeignCurrency
        arr_for_push_sum_value.append([sum_for_week, 'week %s' % week, average, goal, sum_till_this_week])
        arr_for_push_dollar_value.append([dollar_for_week, 'week %s' % week, average, goal, dollar_till_this_week])
        day += 7
    return render(request, 'goals.html', {'arr_for_push_sum_value': arr_for_push_sum_value,
                                          'arr_for_push_dollar_value': arr_for_push_dollar_value})


import calendar


def goals_for_year(request):
    location = request.session['location']
    goals = Goals.objects.all().order_by('-id')[:2]
    reversed_goal = reversed(goals)
    current_year = request.POST.get('year')
    month = 1
    arr_for_push_sum_value = []
    arr_for_push_dollar_value = []
    sum_till_this_week = 0
    dollar_till_this_week = 0
    average = 0
    goal = 0
    for n in reversed_goal:
        if n.name == 'Goal':
            goal += n.value
        elif n.name == 'Average':
            average += n.value
    while month <= 12:
        sum_for_month = 0
        dollar_for_month = 0
        payment_obj = Payment.objects.filter(Date__month=month, Date__year=int(current_year), location=location)
        for payment_obj in payment_obj:
            sum_till_this_week += payment_obj.DefaultCurrency
            sum_for_month += payment_obj.ForeignCurrency
            dollar_for_month += payment_obj.DefaultCurrency
            dollar_till_this_week += payment_obj.ForeignCurrency
        arr_for_push_sum_value.append([sum_for_month, '%s' % calendar.month_name[month], average, goal,
                                       sum_till_this_week])
        arr_for_push_dollar_value.append([dollar_for_month, '%s' % calendar.month_name[month], average, goal,
                                          dollar_till_this_week])
        month += 1
    return render(request, 'goals.html', {'arr_for_push_sum_value': arr_for_push_sum_value,
                                          'arr_for_push_dollar_value': arr_for_push_dollar_value})


def treatment(request):
    location = request.session['location']
    dollar = request.POST.get('treatment_dollar')
    price_category = PriceCategory.objects.filter(location=location)
    payment = Payment.objects.filter(location=location)
    arr_for_push_sum_value = []
    arr_for_push_dollar_value = []
    for price_cat in price_category:
        treatment_sum = 0
        treatment_dollar = 0
        price = Price.objects.filter(CatID=price_cat.ID, location=location)
        for prices in price:
            for payment_obj in payment:
                if payment_obj.TreatId == prices.Kod:
                    treatment_sum += payment_obj.DefaultCurrency
                    treatment_dollar += payment_obj.ForeignCurrency
        sum_to_dollar = treatment_sum / int(dollar)
        sum_plus_dollar = sum_to_dollar + treatment_dollar
        dollar_to_sum = treatment_dollar * int(dollar)
        dollar_plus_sum = dollar_to_sum + treatment_sum
        sum_arround = round(sum_plus_dollar)
        dollar_arround = round(dollar_plus_sum)
        split_sum_plus_dollar = str(sum_arround).split(',')
        split_dollar_plus_sum = str(dollar_arround).split(',')
        arr_for_push_sum_value.append([split_dollar_plus_sum[0], price_cat.CatName, treatment_sum])
        arr_for_push_dollar_value.append([split_sum_plus_dollar[0], price_cat.CatName, treatment_dollar])
    return render(request, 'treatment.html', {'arr_for_push_sum_value': arr_for_push_sum_value,
                                              'arr_for_push_dollar_value': arr_for_push_dollar_value})



