from django.contrib import admin
from .models import Sex, Gorod, Oblast, Personal, Patients, Posesenie, PatientProfessia, PatientRabota, Price, \
    TreatTreat, Payment, Documentation, Zurnal, Locations, Goals
# from import_export import resources
# from data_importer.importers import XLSImporter

# class MyXLSImporterModel(XLSImporter):
#      class Meta:
#          model = Posesenie
#
# class PosesenieResource(resources.ModelResource):
#
#     class Meta:
#         model = Posesenie



# class PosesenieAdmin(admin.ModelAdmin):
#     list_filter = ['personal', ]
#
#
#
#
#
# admin.site.register(Sex)
# admin.site.register(Gorod)
# admin.site.register(Kadri)
# admin.site.register(Oblast)
# admin.site.register(Personal)
# admin.site.register(PatientRabota)
# admin.site.register(Patients)
# admin.site.register(Posesenie, PosesenieAdmin)
# admin.site.register(PatientProfessia)
# admin.site.register(Price)
# admin.site.register(TreatTreat)
# admin.site.register(Payment)
admin.site.register(Locations)
admin.site.register(Goals)
