from django.test import LiveServerTestCase
from django.test import TestCase
from django.contrib.auth.models import User
from selenium import webdriver


class UserLoginTest(TestCase):
    def setUp(self):
        User.objects.create_superuser(username='iliyaz', password='12345678', email='kazikhodzhaev@gmail.com')
        User.objects.create(username='islomjon', password='1234567', email='kaz@gmail.com')

    def test_user_is_supper(self):
        user = User.objects.get(username='iliyaz')
        self.assertEqual(user, 'The user is supper user')


class PollsTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_create_new_poll_via_admin_site(self):
        # Gertrude opens her web browser, and goes to the admin page
        self.browser.get(self.live_server_url + '/admin/')

        # She sees the familiar 'Django administration' heading
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Django administration', body.text)

        # TODO: use the admin site to create a Poll
        self.fail('finish this test')