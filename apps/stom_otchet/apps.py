from django.apps import AppConfig


class StomOtchetConfig(AppConfig):
    name = 'stom_otchet'
