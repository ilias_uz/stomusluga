# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Sex(models.Model):
    sex = models.CharField(max_length=200, verbose_name='')

    def __unicode__(self):
        return self.sex

    class Meta:
        db_table = 'sex'


class Oblast(models.Model):
    oblast = models.CharField(max_length=200, verbose_name='')

    def __unicode__(self):
        return self.oblast

    class Meta:
        db_table = 'oblast'


class Gorod(models.Model):
    gorod = models.CharField(max_length=200, verbose_name='')
    oblast = models.ForeignKey(Oblast, db_column='oblast')

    def __unicode__(self):
        return self.gorod

    class Meta:
        db_table = 'gorod'


class PatientRabota(models.Model):
    rabota = models.CharField(max_length=200, verbose_name='Работа')

    def __unicode__(self):
        return self.rabota

    class Meta:
        db_table = 'patientsrabota'


class PatientProfessia(models.Model):
    professiaId = models.AutoField(primary_key=True)
    professia = models.CharField(max_length=200, verbose_name='Работа')

    def __unicode__(self):
        return self.professia

    class Meta:
        db_table = 'patientsprofessia'


class Personal(models.Model):
    ss_id = models.AutoField(primary_key=True)
    location = models.CharField(max_length=200, db_column='Location')
    DID = models.IntegerField()
    DF = models.CharField(max_length=200, verbose_name='Фамилия доктора')
    DI = models.CharField(max_length=200, verbose_name='Имя доктора')
    DO = models.CharField(max_length=200, verbose_name='Очество доктора', blank=True)
    DProfessia = models.CharField(max_length=300, verbose_name='Профессия доктора', blank=True)
    DBD= models.DateTimeField(verbose_name='День рождение доктора')
    Sex = models.IntegerField(verbose_name='Пол', db_column='sex')
    Nachal = models.DateTimeField(verbose_name='Дата начала работы')
    Uliza = models.CharField(max_length=200, verbose_name='Улица', blank=True)
    Dom = models.CharField(max_length=200, verbose_name='Дом', blank=True)
    Kvartira = models.CharField(max_length=200, verbose_name='Квартира', blank=True)
    TelD = models.CharField(max_length=200, verbose_name='Домашний тел номер', blank=True)
    TelR = models.CharField(max_length=200, verbose_name='Рабочий тел номер', blank=True)
    Gorod = models.CharField(max_length=200, db_column='gorod', verbose_name='Город', blank=True)
    TelM = models.CharField(max_length=200, verbose_name='Мобильный тел', blank=True)
    isDoctor = models.BooleanField(max_length=200, verbose_name='Это доктор')
    DeloN = models.CharField(max_length=200, verbose_name='Номер дела')
    Notes = models.TextField(verbose_name='Заметки', blank=True)
    Photo = models.TextField(verbose_name='Photo')
    ShirtName = models.CharField(max_length=50, db_column='ShirtName')

    def __unicode__(self):
        return self.DI

    class Meta:
        db_table = 'personal'


class Kadry(models.Model):
    ss_id = models.AutoField(primary_key=True)
    ID = models.IntegerField(db_column='ID')
    DID = models.IntegerField(db_column='DID')
    Real = models.BooleanField(db_column='Real')
    location = models.CharField(max_length=200, db_column='Location')

    class Meta:
        db_table = 'kadry'


class Patients(models.Model):
    pid = models.AutoField(primary_key=True)
    gorod = models.ForeignKey(Gorod, db_column='gorod', verbose_name='Город', blank=True)
    uliza = models.CharField(max_length=200, verbose_name='Улица', blank=True)
    pf = models.CharField(max_length=200, verbose_name='')
    pi = models.CharField(max_length=200, verbose_name='')
    nachal = models.DateTimeField(verbose_name='')
    po = models.CharField(max_length=200, verbose_name='')
    pnk = models.CharField(max_length=200, verbose_name='')
    pbd = models.DateTimeField(verbose_name='')
    kvartira = models.CharField(max_length=200, verbose_name='Квартира', blank=True)
    dom = models.CharField(max_length=200, verbose_name='Дом', blank=True)
    rabota = models.ForeignKey(PatientRabota, verbose_name='', db_column='rabota')
    professia = models.ForeignKey(PatientProfessia, verbose_name='', db_column='professia')
    teld = models.CharField(max_length=200, verbose_name='Домашний тел номер', blank=True)
    telr = models.CharField(max_length=200, verbose_name='Рабочий тел номер', blank=True)
    telm = models.CharField(max_length=200, verbose_name='Мобильный тел', blank=True)
    sex = models.ForeignKey(Sex, verbose_name='Пол', db_column='sex')
    # photo = models.CharField(max_length=200, verbose_name='')
    notes = models.TextField()

    def __unicode__(self):
        return self.pi

    class Meta:
        db_table = 'patients'


class Posesenie(models.Model):
    ss_id = models.AutoField(primary_key=True)
    location = models.CharField(max_length=200, db_column='Location')
    ID = models.IntegerField(db_column='ID')
    Personal = models.IntegerField(verbose_name=u'Персонал', db_column=u'personal')
    Patient = models.IntegerField(verbose_name=u'Патциент', db_column=u'patient')
    Data = models.DateField(verbose_name=u'Время посишение', db_column='data')
    Zaloby = models.TextField()
    Obekt = models.TextField()
    Diagnos = models.TextField()
    Treat = models.TextField()
    Anamnez = models.TextField()
    Narad = models.BooleanField()
    summa = models.IntegerField(blank=True)
    SummaDollar = models.IntegerField()

    def __unicode__(self):
        return self.patient.pi

    class Meta:
        db_table = 'posesenie'


class Price(models.Model):
    ss_id = models.AutoField(primary_key=True, db_column='ss_id')
    ID = models.IntegerField(db_column='ID')
    location = models.CharField(max_length=200, db_column='Location')
    Kod = models.CharField(max_length=200, verbose_name='kod')
    TreatName = models.TextField(verbose_name='treat name', db_column='TreatName')
    Price = models.DecimalField(decimal_places=2, max_digits=10)
    PriceDollar = models.DecimalField(decimal_places=2, max_digits=10)
    CatID = models.IntegerField(db_column='CatID')
    isReal = models.BooleanField(db_column='isReal')

    class Meta:
        db_table = 'prices'


class TreatTreat(models.Model):
    ss_id = models.AutoField(primary_key=True, db_column='ss_id')
    ID = models.IntegerField(db_column='ID')
    location = models.CharField(max_length=200, db_column='Location')
    Posesenie = models.IntegerField()
    Treat = models.IntegerField(Price)
    Zub = models.CharField(verbose_name='zub', max_length=200)
    Skolko = models.IntegerField(verbose_name='сколько')
    PS = models.BooleanField(verbose_name='ps')
    Status = models.IntegerField()

    def __unicode__(self):
        return self.treat.kod

    class Meta:
        db_table = 'treattreat'


class Payment(models.Model):
    ss_id = models.AutoField(primary_key=True)
    location = models.CharField(max_length=200, db_column='Location')
    ID = models.IntegerField(db_column='ID')
    Date = models.DateField(db_column='Date')
    PID = models.IntegerField()
    TreatId = models.CharField(max_length=200)
    Discount = models.IntegerField(db_column='Discount')
    Total = models.IntegerField(db_column='Total')
    TotalDollar = models.IntegerField(db_column='TotalDollar')
    Posesenie = models.IntegerField()
    DefaultCurrency = models.IntegerField(db_column='DefaultCurrency')
    ForeignCurrency = models.IntegerField(db_column='ForeignCurrency')
    Method = models.CharField(max_length=200, db_column='Method')

    def __unicode__(self):
        return unicode(self.Posesenie)

    class Meta:
        db_table = 'payment'


class Documentation(models.Model):
    title = models.CharField(max_length=200, db_column='DName')
    description = models.TextField(db_column='DFullText')

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'stomotchety_documentation'


# Doctors

class Chair(models.Model):
    chair = models.CharField(max_length=300)

    def __unicode__(self):
        return self.chair

    class Meta:
        db_table = 'chairs'


class Zurnal(models.Model):
    time = models.DateField(max_length=200, db_column='time')
    period = models.IntegerField(db_column='period')
    chair = models.ForeignKey(Chair, db_column='DID')
    zap = models.CharField(max_length=500, db_column='zap')
    dataZap = models.CharField(max_length=200, db_column='dataZap')
    pid = models.ForeignKey(Patients, db_column='PID')
    ps = models.CharField(max_length=500, db_column='PS')
    zapTag = models.CharField(max_length=300)
    doctor = models.CharField(max_length=50)

    class Meta:
        db_table = 'zurnal'


class Locations(models.Model):
    location = models.CharField(max_length=255, db_column='location')

    def __unicode__(self):
        return self.location

    class Meta:
        db_table = 'locations'


class Goals(models.Model):
    location = models.ForeignKey(Locations, db_column='Location')
    name = models.CharField(max_length=255)
    value = models.DecimalField(max_digits=15, decimal_places=2)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'goals'


class PriceCategory(models.Model):
    ss_id = models.AutoField(primary_key=True)
    location = models.CharField(max_length=200, db_column='Location')
    ID = models.IntegerField(db_column='ID')
    CatName = models.CharField(max_length=255)

    def __unicode__(self):
        return self.CatName

    class Meta:
        db_table = 'prices_category'